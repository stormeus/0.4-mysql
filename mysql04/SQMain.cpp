/*
	SQFuncs.c
	This file is part of mysql04 and adapted from lu_mysql.

	--------------------------------------------------

	Copyright (c) 2011, Juppi/Liberty Unleashed
	Copyright (c) 2014, Stormeus/Vice City: Multiplayer
	All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
		* Redistributions of source code must retain the above copyright
		  notice, this list of conditions and the following disclaimer.
		* Redistributions in binary form must reproduce the above copyright
		  notice, this list of conditions and the following disclaimer in the
		  documentation and/or other materials provided with the distribution.
		* Neither the name of lu_mysql nor the names of its contributors may
		  be used to endorse or promote products derived from this software
		  without specific prior written permission.

	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
	DISCLAIMED. IN NO EVENT SHALL JUPPI/LIBERTY UNLEASHED BE LIABLE FOR ANY
	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "SQImports.h"
#include "SQMain.h"
#include "SQFuncs.h"
#include <stdio.h>

HSQAPI sq;
HSQUIRRELVM v;
PluginFuncs * gFuncs;

void OnSquirrelScriptLoad()
{
	// See if we have any imports from Squirrel
	size_t size;
	int sqId = gFuncs->FindPlugin("SQHost2");
	const void ** sqExports = gFuncs->GetPluginExports(sqId, &size);

	// We do!
	if (sqExports != NULL && size > 0)
	{
		// Cast to a SquirrelImports structure
		SquirrelImports ** sqDerefFuncs = (SquirrelImports **)sqExports;

		// Now let's change that to a SquirrelImports pointer
		SquirrelImports * sqFuncs = (SquirrelImports *)(*sqDerefFuncs);

		// Now we get the virtual machine
		if (sqFuncs)
		{
			// Get a pointer to the VM and API
			sq = *(sqFuncs->GetSquirrelAPI());
			v = *(sqFuncs->GetSquirrelVM());

			// Register functions
			RegisterFuncs(v);
		}
	}
	else
		printf("Failed to attach to SQHost2.\n");
}

uint8_t OnInternalCommand(unsigned int uCmdType, const char* pszText)
{
	switch (uCmdType)
	{
		case 0x7D6E22D8:
			OnSquirrelScriptLoad();
			break;

		default:
			break;
	}

	return 1;
}

extern "C" EXPORT unsigned int VcmpPluginInit(PluginFuncs* givenPluginFuncs, PluginCallbacks* givenPluginCalls, PluginInfo* givenPluginInfo)
{
    givenPluginInfo->apiMajorVersion = PLUGIN_API_MAJOR;
    givenPluginInfo->apiMinorVersion = PLUGIN_API_MINOR;

	gFuncs = givenPluginFuncs;
	givenPluginCalls->OnPluginCommand = OnInternalCommand;
	return 1;
}
