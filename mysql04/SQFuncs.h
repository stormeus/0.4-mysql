//
// SQFuncs: These are the tools to register custom functions within the Squirrel VM.
//
//	Written for Liberty Unleashed by the Liberty Unleashed Team.
//

#pragma once

#include "SQMain.h"
#include "SQImports.h"
#include "squirrel.h"

#ifdef WIN32
	#include <winsock2.h>
#endif
#include "mysql/include/mysql.h"

#define _SQUIRRELDEF( x ) SQInteger x( HSQUIRRELVM v )

#ifdef __cplusplus
extern "C" {
#endif

	SQInteger				RegisterSquirrelFunc				( HSQUIRRELVM v, SQFUNCTION f, const SQChar* fname, unsigned char uiParams, const SQChar* szParams );
	void					RegisterFuncs						( HSQUIRRELVM v );

#ifdef __cplusplus
}
#endif
