//
// SQMain: This is where the module is loaded/unloaded and initialised.
//
//	Written for Liberty Unleashed by the Liberty Unleashed Team.
//

#pragma once

#include <string.h>
#include "SQModule.h"
#include "plugin.h"

#ifdef _WIN32
	#define EXPORT __declspec( dllexport )
	#define _WINSOCKAPI_

	#include <windows.h>
#else
	#define EXPORT
#endif